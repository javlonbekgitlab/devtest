// import strawberry from '../assets/img/strawberry.svg'
// import limon from '../assets/img/limoncard.svg'
// import grape from '../assets/img/grapeSmall.svg'
// import oreo from '../assets/img/oreo.svg'
// import peach from '../assets/img/peach.png'
// import cabbage from '../assets/img/cabbage.svg'
// import avacado from '../assets/img/avacado.svg'
// import tomato from '../assets/img/tomato.svg'
// import potato from '../assets/img/potato.svg'
// import carrot from '../assets/img/carrot.svg'
import kolbasa from '../assets/img/kolbasa.svg'
// import drink from '../assets/img/drink.svg'
// import pineApple from '../assets/img/pineApple.svg'
// import bread from '../assets/img/bread.svg'
// import meet from '../assets/img/meet.svg'
// import kebab from '../assets/img/kebab.svg'
// import cowMeet from '../assets/img/cow-meet.svg'
// import chicken from '../assets/img/chicken.svg'
import milkProd from '../assets/icons/milk.svg'
import vegetablesIcon from '../assets/icons/vegetables.svg'
import fruitsIcon from '../assets/icons/fruits.svg'
import drinksIcon from '../assets/icons/drinks.svg'
import sprices from '../assets/icons/salt.svg'
import meatProds from '../assets/icons/meat.svg'
import oil from '../assets/icons/oil.svg'
import { lang } from '../language/lang'
import { observable , action} from 'mobx'
import { carouselTransformer } from './transformers'


// export const fruits = [
//     {
//       img: grape,
//     },
//     {
//       img: strawberry,
//     },
//     {
//       img: limon,
//     },
//     {
//       img: oreo,
//     },
//     {
//       img: peach,
//     },
//     {
//       img: grape,
//     },
//     {
//       img: strawberry,
//     },
//     {
//       img: limon,
//     },
//     {
//       img: oreo,
//     },
//     {
//       img: peach,
//     }
// ]

// export const vegetables = [
//     {
//         img: cabbage
//     },
//     {
//         img: avacado
//     },
//     {
//         img: tomato
//     },
//     {
//         img: potato
//     },
//     {
//         img: carrot
//     },
//     {
//         img: cabbage
//     },
//     {
//         img: avacado
//     },
//     {
//         img: tomato
//     },
//     {
//         img: potato
//     },
//     {
//         img: carrot
//     },
// ]

// export const meetProducts = [
//     {
//         img: kolbasa
//     },
//     {
//         img: meet
//     },
//     {
//         img: kebab
//     },
//     {
//         img: cowMeet
//     },
//     {
//         img: chicken
//     },
//     {
//         img: kolbasa
//     },
//     {
//         img: meet
//     },
//     {
//         img: kebab
//     },
//     {
//         img: cowMeet
//     },
//     {
//         img: chicken
//     },
// ]

export const carouselCards = observable({
    data: [
        // {
        //     image: peach,
        //     italicText: 'Персик с полей нашего солнечного края.',
        //     text: 'Вкус лето, круглый год'
        // },
        // {
        //     image: tomato,
        //     italicText: 'Помидор с полей нашего солнечного края.',
        //     text: 'Вкус Помидора, круглый год'
        // },
        // {
        //     image: grape,
        //     italicText: 'Виноград с полей нашего солнечного края.',
        //     text: 'Вкус Винограда, круглый год'
        // },
        // {
        //     image: oreo,
        //     italicText: 'Орэо с полей нашего солнечного края.',
        //     text: 'Вкус Орэоа, круглый год'
        // }
    ],
    cart: [],
    activeCard: {},
    setData: action(function(data) {
        this.data = carouselTransformer(data)
        this.setActiveCard()
    }),
    setActiveCard: action(function(){
        if (this.activeCard.active < this.activeCard.quantity) {
            this.activeCard = {...this.activeCard, ...this.data[this.activeCard.active]}
            this.activeCard.active = this.activeCard.active + 1
            this.activeCard.quantity = this.activeCard.quantity
                
        } else {
            this.activeCard = this.data[0]
            this.activeCard.active = 1
            this.activeCard.quantity = this.data.length
    }}),
    setCart: action(function(newData) {
        console.log(newData)
        this.cart = [...this.cart, newData]
    }),
    deleteItemFromCart: action(function(id){
        this.cart = this.cart.filter((item) => item.id !== id)
    })
})

export const catalogProducts = [
    {
        icon: milkProd,
        text: lang.milkProd
    },
    {
        icon: vegetablesIcon,
        text: lang.vegetables
    },
    {
        icon: fruitsIcon,
        text: lang.fruits
    },
    {
        icon: drinksIcon,
        text: lang.drinks
    },
    {
        icon: sprices,
        text: lang.spices
    },
    {
        icon: meatProds,
        text: lang.meatProd
    },
    {
        icon: oil,
        text: lang.oil
    },
]
export const catalog = observable({
    data: [],
    setData: action(function(data) {
        this.data = data
    })
})

// export const discountingProducts = [
//     {
//         title: 'Сок Dena (Мохито)',
//         img: drink,
//         price: '10.990',
//         discountPrice: '12.990',
//         currency: 'сум'  
//     },
//     {
//         title: 'Ананас',
//         img: pineApple,
//         price: '10.990',
//         discountPrice: '12.990',
//         currency: 'сум'  
//     },
//     {
//         title: 'Хлеб',
//         img: bread,
//         price: '10.990',
//         discountPrice: '12.990',
//         currency: 'сум'  
//     },
//     {
//         title: 'Сок Dena (Мохито)',
//         img: drink,
//         price: '10.990',
//         discountPrice: '12.990',
//         currency: 'сум'  
//     },
//     {
//         title: 'Хлеб',
//         img: bread,
//         price: '10.990',
//         discountPrice: '12.990',
//         currency: 'сум'  
//     },
//     {
//         title: 'Сок Dena (Мохито)',
//         img: drink,
//         price: '10.990',
//         discountPrice: '12.990',
//         currency: 'сум'  
//     },
//     {
//         title: 'Хлеб',
//         img: bread,
//         price: '10.990',
//         discountPrice: '12.990',
//         currency: 'сум'  
//     },
//     {
//         title: 'Сок Dena (Мохито)',
//         img: drink,
//         price: '10.990',
//         discountPrice: '12.990',
//         currency: 'сум'  
//     },
//     {
//         title: 'Хлеб',
//         img: bread,
//         price: '10.990',
//         discountPrice: '12.990',
//         currency: 'сум'  
//     },
//     {
//         title: 'Сок Dena (Мохито)',
//         img: drink,
//         price: '10.990',
//         discountPrice: '12.990',
//         currency: 'сум'  
//     },
//     {
//         title: 'Хлеб',
//         img: bread,
//         price: '10.990',
//         discountPrice: '12.990',
//         currency: 'сум'  
//     },
//     {
//         title: 'Сок Dena (Мохито)',
//         img: drink,
//         price: '10.990',
//         discountPrice: '12.990',
//         currency: 'сум'  
//     },
//     {
//         title: 'Хлеб',
//         img: bread,
//         price: '10.990',
//         discountPrice: '12.990',
//         currency: 'сум'  
//     }
// ]

export const cartProducts = [
    {
        title: 'Сок Dena (Мохито)',
        img: kolbasa,
        price: '10.990',
        discountPrice: '12.990',
        currency: 'сум',
        dimension: 'шт'  
    },
    {
        title: 'Ананас',
        img: kolbasa,
        price: '10.990',
        discountPrice: '12.990',
        currency: 'сум',
        dimension: 'кг'  
    }
]