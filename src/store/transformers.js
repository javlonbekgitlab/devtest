export const carouselTransformer = data => {
    if(Array.isArray(data)) {
        const newData = data.map(item => {
            item.italicText = item.description
            item.text = item.name
            // item.id = item.id
            // item.price = item.price
            // item.image = item.image
            return item
        })
        return newData
    } else {
        return []
    }
}