import { carouselCards } from "."
import { Api } from "../api"
import { token } from "../api/constants"

export const getDataFromServer = async ({host, url, setData}) => {
    const res = await Api({host, url, type: 'get', token})
    if (res.status === 200) {
        console.log(res.data)
        carouselCards.setData(res.data.data)
    } else {
        alert(res)
    }
}