import React, { useState } from 'react'
import { lang } from '../../language/lang'
import arrowDown from '../../assets/icons/arrowDown.svg'
import { Button } from '../button'
import styles from './index.module.sass'
import { catalogProducts, discountingProducts } from '../../store'
import { DiscountItem } from '../discountItem'
import { BackgroundImages } from '../backgroundImages'

export const DiscountBlock = ({data}) => {

    const [activeCard, setActiveCard] = useState(undefined)

    const [showCategory, setShowCategory] = useState(false)

    const handleHover = index => setActiveCard(index)

    const handleLeave = () => setActiveCard(undefined)

    const handleSetShowCategory = () => setShowCategory(prevState => !prevState)

    return (
        <div className = {styles.cont}>
            <BackgroundImages/>
            <div className = {styles.contentCont}>
                <div className = {styles.catalogCont}>
                    {/* <div className = {styles.hambugerCont}>
                        <Button>
                            <img src = {hambuger} alt = 'hambuger'/>
                        </Button>
                    </div> */}
                    <div className = {styles.catalogContent}>
                        <Button onClick = {handleSetShowCategory}>{lang.catalog}</Button>
                        <div className = {showCategory ? `${styles.listCont} ${styles.listContShow}` : `${styles.listCont} ${styles.listContHide}`}>
                            {catalogProducts.map(item => 
                                <div key = {item.text} className = {styles.listItem}>
                                    <div className = {styles.iconCont}>
                                        <img className = {styles.icon} src = {item.icon} alt = {item.text}/>
                                    </div>
                                    <div className = {styles.text}>{item.text}</div>
                                </div>
                            )}
                        </div>
                    </div>
                </div>
                <div className = {styles.content}>
                    <div className = {styles.titleCont}>
                        <div className = {styles.title}>{lang.discountTitle}</div>
                        <div className = {styles.secondaryTitle}>{lang.discountSecondaryTitle}</div>
                    </div>
                    <div className = {styles.discountItems}>
                        {data.length > 0 && data.map((item, index) => 
                            <DiscountItem 
                                onHover = {() => handleHover(index)} 
                                isActive = {activeCard === index}
                                onLeave = {handleLeave}
                                key = {item.id}
                                underActive = {(activeCard + 3) === index}
                                {...item}
                            />    
                        )}
                    </div>
                    <div className = {styles.showMoreCont}>
                        <Button>
                            {lang.showMore}
                        </Button>
                        <div className = {styles.arrowCont}>
                            <img className = {styles.arrowDown} src = {arrowDown} alt = 'show more'/>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}