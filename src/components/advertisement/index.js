import React from 'react'
import { lang } from '../../language/lang'
import styles from './index.module.sass'
import delivery from '../../assets/img/delivery.svg' 
import deliveryHeavy from '../../assets/img/deliveryHeavy.svg' 
import discount from '../../assets/img/discountAdv.svg' 
import chooseTheBest from '../../assets/img/chooseBest.svg' 

export const Advertisement = () => {
    return (
        <div className = {styles.cont}>
            <div className = {styles.title}>{lang.advertisementTitle}</div>
            <div className = {styles.content}>
                <div className = {styles.column}>
                    <div className = {styles.item}>
                        <img className = {styles.itemImg} src = {delivery} alt = 'delivery'/>
                        <div className = {styles.itemTitle}>{lang.deliveryDescription}</div>
                    </div>
                    <div className = {styles.item}>
                        <img className = {styles.itemImg} src = {deliveryHeavy} alt = 'delivery'/>
                        <div className = {styles.itemTitle}>{lang.deliveryHeavy}</div>
                    </div>
                </div>
                <div className = {styles.column}>
                    <div className = {styles.item}>
                        <img className = {styles.itemImg} src = {chooseTheBest} alt = 'delivery'/>
                        <div className = {styles.itemTitle}>{lang.chooseBest}</div>
                    </div>
                    <div className = {styles.item}>
                        <img className = {styles.itemImg} src = {discount} alt = 'delivery'/>
                        <div className = {styles.itemTitle}>{lang.discountAdv}</div>
                    </div>
                </div>
            </div>
        </div>
    )
}