import React from 'react'
import styles from './index.module.sass'
import GoogleMapReact from 'google-map-react'

export const Map = ({
    center = {
        lat: 59.95,
        lng: 30.33,
    },
    zoom = 11,
    children
}) => {
    return (
        <div className = {styles.cont}>
            <GoogleMapReact
                bootstrapURLKeys={{ key: 'AIzaSyAs8FcePD4bUCdZBjkeCfDRnmvTRlEdNHA' }}
                defaultCenter={center}
                defaultZoom={zoom}
            >
                {!children && <div lat = {41.311081} lng = {69.240562}>*</div> }
            </GoogleMapReact>
        </div>
    )
}