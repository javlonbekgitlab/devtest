import React from 'react'
import styles from './index.module.sass'

export const GreenTitle = ({children}) => {
    return <div className = {styles.sliderTitle}>{children}</div>
      
}