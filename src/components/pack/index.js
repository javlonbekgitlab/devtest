import React from 'react'
import styles from './index.module.sass'
import pack from '../../assets/icons/pack.svg'

export const Pack = ({discount, onClick}) => {
    return (
        <div onClick = {onClick} className = {discount ? `${styles.packCont} ${styles.discount}` : styles.packCont}>
            <img className = {discount ?  `${styles.pack} ${styles.discountIcon}` : styles.pack} src = {pack} alt = 'pack'/>
        </div>
    )
}