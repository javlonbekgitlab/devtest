import React, { useCallback, useState, memo } from 'react'
import styles from './index.module.sass'
import SearchIcon from '@mui/icons-material/Search'
import FavoriteBorderIcon from '@mui/icons-material/FavoriteBorder'
import ShoppingCartIcon from '@mui/icons-material/ShoppingCart'
import MenuIcon from '@mui/icons-material/Menu'
import Menu from '@mui/material/Menu'
import MenuItem from '@mui/material/MenuItem'
import LogoutIcon from '@mui/icons-material/Logout'
import KeyboardArrowDownIcon from '@mui/icons-material/KeyboardArrowDown'
// import logoLebazar from '../../assets/logo/leBazarLogo.svg'
import logoBozorbek from '../../assets/logo/Bozorbek.svg'
import logoKorzinka from '../../assets/logo/korzinka.png'
import { lang } from '../../language/lang'
import { IconButton } from '@mui/material'
import { Input } from '../input'
import { Link } from 'react-router-dom'

export const Header = memo(({activeLang = 'en'}) => {

    const [showSearch, setShowSearch] = useState(false)

    const [anchorEl, setAnchorEl] = React.useState(null)
    
    const open = Boolean(anchorEl)
    
    const handleClick = (event) => {
        setAnchorEl(event.currentTarget)
        console.log('open')
    }
    const handleClose = () => {
        setAnchorEl(null)
    }

    const handleSetShowSearch = useCallback(() => {
        setShowSearch(prevState => !prevState)
    }, [showSearch, setShowSearch])

    return (
        <div className = {styles.cont}>
            <div className = {styles.logo}>
                <Link to = '/'>
                    <img
                        key = "uniqieasdafsdjfhg321"
                        className = {styles.imgLogo} 
                        src = {logoBozorbek} 
                        alt = 'logo'
                    />
                </Link>
            </div>
            <div className = {styles.navCont}>
                <div className = {styles.navItem}>{lang.products}</div>
                <div className = {styles.navItem}>{lang.delivery}</div>
                <div className = {styles.navItem}>{lang.payment}</div>
                <div className = {styles.navItem}>{lang.reputing}</div>
                <Link to = '/contacts'>
                    <div className = {styles.navItem}>{lang.contancts}</div>
                </Link>
                <div className = {showSearch ? `${styles.inputCont} ${styles.showSearch}` : `${styles.inputCont} ${styles.hideSearch}`}>
                    <Input placeholder = {lang.search}/>
                </div>
                <div className = {styles.searchCont}>
                    <IconButton 
                        onClick = {handleSetShowSearch}
                        className = {styles.buttonCont}
                    >
                        <SearchIcon sx={{ color: '#00EA5D'}}/>
                    </IconButton>
                </div>
            </div>
            <div className = {styles.iconCont}>
                <div>
                    <IconButton>
                        <LogoutIcon sx={{ color: '#00EA5D'}}/>
                    </IconButton>
                </div>
                <div>
                    <Link to = '/cart'>
                        <IconButton>
                            <ShoppingCartIcon sx={{ color: '#00EA5D'}}/>
                        </IconButton>
                    </Link>
                </div>
                <div>
                    <IconButton>
                        <FavoriteBorderIcon sx={{ color: '#00EA5D'}}/>
                    </IconButton>
                </div>
                <div>
                    <IconButton>
                        <div className = {styles.langCont}>
                            <div className = {styles.oval}>{activeLang}</div>
                        </div>
                    </IconButton>
                </div>
            </div>
            <div className = {styles.hamburgerCont}> 
                <Menu
                    id="basic-menu"
                    anchorEl={anchorEl}
                    open={open}
                    className = {styles.menu}
                    onClose={handleClose}
                    // disableAutoFocus={true}
                    onClick = {handleClose}
                    // disableAutoFocusItem = {true}
                    MenuListProps={{
                    'aria-labelledby': 'basic-button',
                    }}
                >           
                    <div className = {styles.navContMini}>
                        <div className = {styles.navItem}>{lang.products}</div>
                        <div className = {styles.navItem}>{lang.delivery}</div>
                        <div className = {styles.navItem}>{lang.payment}</div>
                        <div className = {styles.navItem}>{lang.reputing}</div>
                        <Link to = '/contacts'>
                            <div className = {styles.navItem}>{lang.contancts}</div>
                        </Link>
                        <div className = {styles.iconCont}>
                            <div className = {styles.icons}> 
                                <IconButton>
                                    <LogoutIcon fontSize = 'small' sx={{ color: '#00EA5D'}}/>
                                </IconButton>
                            </div>
                            <Link to = '/cart'>
                                <div className = {styles.icons}>
                                    <IconButton>
                                        <ShoppingCartIcon fontSize = 'small' sx={{ color: '#00EA5D'}}/>
                                    </IconButton>
                                </div>
                            </Link>
                            <div className = {styles.icons}>
                                <IconButton>
                                    <FavoriteBorderIcon fontSize = 'small' sx={{ color: '#00EA5D'}}/>
                                </IconButton>
                            </div>
                            <div className = {styles.icons}>
                                <IconButton>
                                    <div className = {styles.lang}>{activeLang}</div>
                                </IconButton>
                            </div>
                        </div>
                    </div>
                </Menu>
            </div>
            <div className = {styles.searchMiniCont}>
                <div className = {showSearch ? `${styles.inputCont} ${styles.showSearch}` : `${styles.inputCont} ${styles.hideSearch}`}>
                    <Input paddingRight placeholder = {lang.search}/>
                </div>
                <IconButton 
                    onClick = {handleSetShowSearch}
                    className = {styles.buttonCont}
                >
                    <SearchIcon sx={{ color: '#00EA5D'}}/>
                </IconButton>
                <IconButton
                    id="basic-button"
                    aria-controls={open ? 'basic-menu' : undefined}
                    onClick = {handleClick}
                    aria-haspopup="true"
                    aria-expanded={open ? 'true' : undefined}
                >
                    <MenuIcon fontSize = 'small' sx={{ color: '#00EA5D'}}/>
                </IconButton>
            </div>
        </div>
    )
})