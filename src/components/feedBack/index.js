import React from 'react'
import { lang } from '../../language/lang'
import styles from './index.module.sass'
import { Input } from '../input'
import { Button } from '../button'
import airPlane from '../../assets/icons/airPlane.svg'
import ellipse from '../../assets/img/ellipse.svg'
import smallEllipse from '../../assets/img/small-ellipse.svg'
import feedbackGirl from '../../assets/img/feedbackGirl.svg'

export const FeedBack = () => {
    return (
        <div className = {styles.cont}>
            <div className = {styles.left}>
                <div className = {styles.title}>{lang.questions}</div>
                <div className = {styles.answer}>{lang.getAnswer}</div>
                <div className = {styles.form}>
                    <div className = {styles.formItem}>
                        <Input fullFilled placeholder = {lang.interesting}/>
                    </div>
                    <div className = {styles.formItem}>
                        <Input fullFilled placeholder = {lang.email}/>
                    </div>
                    <Button
                        icon = {<img src = {airPlane} alt = 'send'/>}
                    >
                        {lang.send}
                    </Button>
                </div>
            </div>
            <div className = {styles.right}>
                <div className = {styles.imgCont}>
                    <img className = {styles.ellipse} src = {ellipse} alt = 'img'/>
                    <img className = {styles.feedbackGirl} src = {feedbackGirl} alt = 'img'/>
                </div>
                <div className = {styles.shapeCont}>
                    <img className = {styles.smallEllipse} src = {smallEllipse} alt = 'img'/>
                    <div className = {styles.shape}/>
                </div>
            </div>
        </div>
    )
}