import React, { useState } from 'react'
import styles from './index.module.sass'
import minus from '../../assets//icons/minus.svg'
import plus from '../../assets//icons/plus.svg'
import PropTypes from 'prop-types'

export const MinusPlus = ({value = 1, dimension}) => {

    const [quant, setQuant] = useState(1)

    const handleInc = () => setQuant(prevState => prevState + 1)

    const handleDec = () => setQuant(prevState => (prevState > 1) ? (prevState - 1) : 1)

    return (
        <div className = {styles.cont}>
            <div onClick = {handleDec} className = {styles.iconCont}>
                <img className = {styles.img} src = {minus} alt = 'minus'/>
            </div>
            <div className = {styles.content}>
                <div className = {styles.item}>{quant}</div>
                <div className = {styles.item}>{dimension}</div>
            </div>
            <div onClick = {handleInc} className = {styles.iconCont}>
                <img className = {styles.img} src = {plus} alt = 'minus'/>
            </div>
        </div>
    )
}

MinusPlus.propTypes = {
    value: PropTypes.number.isRequired,
    dimension: PropTypes.string.isRequired
}