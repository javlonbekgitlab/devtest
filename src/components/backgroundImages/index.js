import React from 'react'
import styles from './index.module.sass'
import bgLeftImage from '../../assets/img/discountBackGroundLeft.png'
import bgRightImage from '../../assets/img/discountBackGroundRight.png'

export const BackgroundImages = () => {
    return (
        <div className = {styles.background}>
            <img className = {styles.image} src = {bgLeftImage} alt = 'background'/>
            <img className = {styles.image} src = {bgRightImage} alt = 'background'/>
        </div>
    )
}