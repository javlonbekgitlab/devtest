import React from 'react'
import styles from './index.module.sass'

export const Input = ({fullFilled, paddingRight, ...props}) => {
    return (
        <input style = { paddingRight && {paddingRight: '100px'}} className = {fullFilled ? styles.fullFilledStyle : styles.input} {...props}/>
    )
}