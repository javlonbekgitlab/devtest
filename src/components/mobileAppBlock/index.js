import React from 'react'
import styles from './index.module.sass'
import phone from '../../assets/img/phone.png'
import googlePlay from '../../assets/img/googlePlay.svg'
import appStore from '../../assets/img/appStore.svg'
import phoneAndCart from '../../assets/icons/phoneAndCart.svg'
import timeAndTruck from '../../assets/icons/timeAndTruck.svg'
import products from '../../assets/icons/products.svg'
import cartWithBitcoin from '../../assets/icons/cartWithBitcoin.svg'
import { lang } from '../../language/lang'

export const MobileAppBlock = () => {
    return (
        <div className = {styles.cont}>
            <div className = {styles.imgCont}>
                <img className = {styles.img} src = {phone} alt = 'img'/>
            </div>
            <div className = {styles.infoCont}>
                <div className = {styles.title}>{lang.downloadApp}</div>
                <div className = {styles.stepsCont}>
                    <div className = {styles.stepsColumn}>
                        <div className = {styles.columnItem}>
                            <img className = {styles.icon} src = {phoneAndCart} alt = 'icon'/>
                            <div className = {styles.text}>{lang.manual}</div>
                        </div>
                        <div className = {styles.columnItem}>
                            <img className = {styles.icon} src = {timeAndTruck} alt = 'icon'/>
                            <div className = {styles.text}>{lang.courier}</div>
                        </div>
                    </div>
                    <div className = {styles.stepsColumn}>
                        <div className = {styles.columnItem}>
                            <img className = {styles.icon} src = {products} alt = 'icon'/>
                            <div className = {styles.text}>{lang.purchaseSpecialist}</div>
                        </div>
                        <div className = {styles.columnItem}>
                            <img className = {styles.icon} src = {cartWithBitcoin} alt = 'icon'/>
                            <div className = {styles.text}>{lang.safeWithUs}</div>
                        </div>
                    </div>
                </div>
                <div className = {styles.sources}>
                    <img src = {googlePlay} className = {styles.source} alt = 'img'/>
                    <img src = {appStore} className = {styles.source} alt = 'img'/>
                </div>
            </div>
        </div>
    )
}