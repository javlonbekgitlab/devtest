import React, { useEffect, useRef, useState } from 'react'
import { lang } from '../../language/lang'
import { Pack } from '../pack'
import styles from './index.module.sass'

export const DiscountItem = ({
    name, 
    price, 
    discountPrice, 
    image, 
    currency, 
    onHover, 
    onLeave, 
    isActive, 
    underActive
}) => {

    const cont = useRef(null)

    const innerCont = useRef(null)

    const [innerInitialHeight, setInnerInitialHeight] = useState(undefined)

    const [active, setActive] = useState(styles.innerCont)

    const handleHover = () => {
        onHover()
        setActive(`${styles.innerCont}`)
        cont.current.style.boxShadow = 'none'
        // innerCont.current.style.height = `${cont.current.offsetHeight * 2}px`
    }

    const handleHoverLeave = () => {
        onLeave()
        setActive(`${styles.innerCont} ${styles.activeCont}`)
        cont.current.style.boxShadow = '0px 4px 10px rgba(0, 0, 0, 0.25)'
        // innerCont.current.style.height = `${innerInitialHeight}px`

    }

    useEffect(() => {
        setInnerInitialHeight(innerCont.current.offsetHeight)
    }, [])

    useEffect(() => {
        if (isActive) {
            cont.current.style.position = 'relative'    
        } else {
            cont.current.style.position = 'static'
        }
        if (underActive) {
            cont.current.style.boxShadow = 'none'
            cont.current.style.opacity = '0'
        } else {
            cont.current.style.boxShadow = '0px 4px 10px rgba(0, 0, 0, 0.25)'
            cont.current.style.opacity = '1'
        }
    }, [isActive, underActive])

    return (
        <div 
            ref = {cont} 
            onMouseOver = {handleHover}
            onMouseLeave = {handleHoverLeave}
            className = {styles.cont}
        >
            <div 
                ref = {innerCont}
                className = {active}
            >
                <div className = {styles.head}>
                    <div className = {styles.title}>{name}</div>
                    <div className = {styles.content}>
                        {/* <div className = {styles.left}> */}
                        <div className = {styles.discount}>{lang.discount}</div>
                        <div className = {styles.dash}/>
                        <div className = {styles.priceMainCont}>
                            <div className = {styles.priceCont}>
                                <div className = {styles.price}>{price + 100}</div>
                                <div className = {styles.currency}>{currency}</div>
                            </div>
                            <div className = {styles.priceCont}>
                                <div className = {styles.price}>{price}</div>
                                <div className = {styles.currency}>{currency}</div>
                            </div>
                        </div>
                        {/* </div> */}
                    </div>
                </div>
                <div className = {styles.imgCont}>
                    {isActive && <div className = {styles.absoluteDiscount}>{lang.discount}</div>}
                    <img className = {styles.image} src = {image} alt = {name}/>
                    <div className = {styles.packCont}>
                        <Pack discount/>
                    </div>
                </div>
            </div>
        </div>
    )
}