import React from 'react'
import styles from './index.module.sass'
import logo from '../../assets/logo/Bozorbek.svg'
import insta from '../../assets/icons/insta.svg'
import telegram from '../../assets/icons/telegram.svg'
import facebook from '../../assets/icons/facebook.svg'
import googlePlay from '../../assets/logo/google-play.svg'
import appStore from '../../assets/logo/apple.svg'
import { lang } from '../../language/lang'

export const Footer = () => {
    return (
        <div className = {styles.cont}>
            <div className = {styles.column}>
                <img className = {styles.logo} src = {logo} alt = 'logo'/>
                <div className = {styles.text}>{lang.writeQuestion}</div>
                <div className = {`${styles.socialCont} ${styles.hide}`}>
                    <div className = {styles.social}>{lang.social}</div>
                    <div className = {styles.iconsCont}>
                        <a href = ''>
                            <img className = {styles.icon} src = {insta} alt = 'icons'/>
                        </a>
                        <a href = ''>
                            <img className = {styles.icon} src = {telegram} alt = 'icons'/>
                        </a>
                        <a href = ''>
                            <img className = {styles.icon} src = {facebook} alt = 'icons'/>
                        </a>
                    </div>
                </div>
            </div>
            <div className = {styles.column}>
                <div className = {styles.title}>{lang.aboutCompany}</div>
                <div className = {styles.text}>{lang.tenants}</div>
                <div className = {styles.text}>{lang.ourBrands}</div>
                <div className = {styles.text}>{lang.callCenter}</div>
                <div className = {styles.text}>{lang.delivery}</div>
                <div className = {styles.text}>{lang.history}</div>
            </div>
            <div className = {styles.column}>
                <div className = {styles.title}>{lang.contancts}</div>
                <div className = {styles.text}>{lang.aroundTheClock}</div>
                <div className = {styles.text}>{lang.compEmail}</div>
                <div className = {styles.text}>{lang.telNumber}</div>
            </div>
            <div className = {styles.column}>
                <div className = {styles.group}>
                    <img className = {styles.logoPlatform} src = {googlePlay} alt = 'img'/>
                    <div className = {styles.textCont}>
                        <div className = {styles.helperText}>{lang.available}</div>
                        <div className = {styles.platFormName}>{'Google Play'}</div>
                    </div>
                </div>
                <div className = {styles.group}>
                    <img className = {styles.logoPlatform} src = {appStore} alt = 'img'/>
                    <div className = {styles.textCont}>
                        <div className = {styles.helperText}>{lang.downloadIt}</div>
                        <div className = {styles.platFormName}>{'App Store'}</div>
                    </div>
                </div>
            </div>
            <div className = {styles.column}>
                <div className = {styles.socialCont}>
                    <div className = {styles.social}>{lang.social}</div>
                    <div className = {styles.iconsCont}>
                        <a href = ''>
                            <img className = {styles.icon} src = {insta} alt = 'icons'/>
                        </a>
                        <a href = ''>
                            <img className = {styles.icon} src = {telegram} alt = 'icons'/>
                        </a>
                        <a href = ''>
                            <img className = {styles.icon} src = {facebook} alt = 'icons'/>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    )
}