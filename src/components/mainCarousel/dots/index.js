import React from 'react'
import styles from './index.module.sass'
import PropTypes from 'prop-types'

export const Dots = ({quantity, active}) => {

    const dots = () => {
        const elements = []
        for(let i = 0; i < quantity; i++) {
            elements.push(<div className = {`${styles.dot} ${active === (i + 1) && styles.active}`}/>)
        }
        return elements
    }
    return (
        <div className = {styles.cont}>
            {dots().length > 0 && dots().map(value => value)}
        </div>
    )
}

Dots.propTypes = {
    quantity: PropTypes.number.isRequired,
    active: PropTypes.number.isRequired
}