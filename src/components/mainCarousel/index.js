import React, { useEffect, useState } from 'react'
import styles from './index.module.sass'
import grape  from '../../assets/img/grape.png'
import limon  from '../../assets/img/limon.png'
import limonSmall  from '../../assets/img/small-limon.png'
import ananas  from '../../assets/img/ananas.png'
import peach  from '../../assets/img/peach.png'
import { Button } from '../button'
import { lang } from '../../language/lang'
import { Dots } from './dots'
import { BlinkingCursorTextBuilder, FloatingLettersTextBuilder } from 'react-animated-text-builders'
import { PulseLoader } from "react-spinners"
import PropTypes from 'prop-types'
import { inject, observer } from 'mobx-react'
import LazyLoad from 'react-lazyload'
import { LazyLoader } from '../lazyImageLoader'
// import { Carousel } from '../carousel'

export const MainCarousel = inject('carouselCards')(observer(({carouselCards}) => {

    const [activeClass, setActiveClass] = useState({
        image: styles.image,
        text: styles.text
    })
    // const [activeCard, setActiveCard] = useState({
    //     image: cards[0]?.image,
    //     quantity: cards.length,
    //     active: 1,
    //     italicText: cards[0]?.italicText,
    //     text: cards[0]?.text
    // })  

    // function animate() {
    //     if (text.length === 0) {
    //         setText('gfg fh fhfh sdf sd fsdfsdf asd'.split(''))
    //     } else {

    //         let testText = text
    //         console.log(text.length)
    //         testText.pop()
    //         setText(testText)
    //     }
    // }
    // (function animate() {
    //     let testText = text.split('')
    //     console.log(testText.shift())
    //     // testText.length > 0 ?  setText(prevState => prevState + testText.shift()) : clearTimeout(running); 
    //     // var running = setTimeout(animate, 1000);
    // })()

    // useEffect(() => setInterval(() => {
    //     animate()
    // }, 500), [])
        
    useEffect(() => {
        setInterval(() => {
            // setActiveCard(({active, quantity}) => {
            //     // if (true) {
            //     //     return ({
            //     //         ...prevState,
            //     //         active: prevState.active + 1,
            //     //         quantity: prevState.quantity + 1
            //     //     })
            //     // }
            //     if (active < quantity) {
            //         return ({
            //             ...cards[active],
            //             active: active + 1,
            //             quantity
            //         })
            //     } else {
            //         return ({
            //             ...cards[0],
            //             active: 1,
            //             quantity
            //         })
            //     }
            // })
            carouselCards.setActiveCard()
            setActiveClass({
                image: `${styles.image} ${styles.activeClass}`,
                text: `${styles.text} ${styles.activeClassText}`
            })
            setTimeout(() => {
                setActiveClass({
                    image: `${styles.image}`,
                    text: `${styles.text}`
                })
            }, 1000)
        }, 2000)
    }, [])

    return (
        <div className = {styles.cont}>
            <div className = {styles.background}>
                <div className = {styles.backgroundItem}>
                    <LazyLoad>
                        <img className = {styles.bgImage} src = {grape} alt = 'grape'/>
                    </LazyLoad>
                    <LazyLoad>
                        <img className = {styles.bgImage} src = {limon} alt = 'limon'/>
                    </LazyLoad>
                </div>
                <div className = {styles.backgroundItem}>
                    <img className = {styles.bgImageSmall} id = {styles.smallLimon} src = {limonSmall} alt = 'limon'/>
                    <img className = {styles.bgImage} src = {ananas} alt = 'ananas'/>
                </div>
            </div>
            {carouselCards.data.length > 0 ? <div className = {styles.carouselCont}>
                <div className = {styles.carousel}>
                    <div className = {activeClass.text}>
                        <div className = {styles.italic}>{carouselCards.activeCard.italicText}</div>
                        <div className = {styles.bold}>{carouselCards.activeCard.text}</div>
                        <Button>{lang.buyNow}</Button>
                    </div>
                    <LazyLoader>
                        <img className = {activeClass.image} src = {carouselCards.activeCard.image} alt = 'photomain'/>
                    </LazyLoader>
                </div>
                <Dots quantity = {carouselCards.activeCard.quantity} active = {carouselCards.activeCard.active}/>
            </div> : <div><PulseLoader color = '#00EA5D'/></div>}
        </div>
    )
}))

// MainCarousel.propTypes = {
//     cards: [{
//         image: PropTypes.object,
//         italicText: PropTypes.string,
//         text: PropTypes.string
//     }]
// }
