import React, { useState } from 'react'
import styles from './index.module.sass'
import grape  from '../../assets/img/grape.png'
import limon  from '../../assets/img/limon.png'
import limonSmall  from '../../assets/img/small-limon.png'
import ananas  from '../../assets/img/ananas.png'
// import peach  from '../../assets/img/peach.png'
import { Button } from '../button'
import { lang } from '../../language/lang'
import { Dots } from './dots'
import { PulseLoader } from "react-spinners"
import { inject, observer } from 'mobx-react'
import "react-responsive-carousel/lib/styles/carousel.min.css"
import { LazyLoader } from '../lazyImageLoader'
import { Carousel } from 'react-responsive-carousel'
// import { Carousel } from '../carousel'
// import FastAverageColor from 'fast-average-color'

export const ResponsiveCarousel = inject('carouselCards')(observer(({carouselCards}) => {

    const [active, setActive] = useState(1) 

    const handleSetActive = active => setActive(active + 1)

    return (
        <div className = {styles.cont}>
            <div className = {styles.background}>
                <div className = {styles.backgroundItem}>
                    <LazyLoader>
                        <img className = {styles.bgImage} src = {grape} alt = 'grape'/>
                    </LazyLoader>
                    <LazyLoader>
                        <img className = {styles.bgImage} src = {limon} alt = 'limon'/>
                    </LazyLoader>
                </div>
                <div className = {styles.backgroundItem}>
                    <img className = {styles.bgImageSmall} id = {styles.smallLimon} src = {limonSmall} alt = 'limon'/>
                    <img className = {styles.bgImage} src = {ananas} alt = 'ananas'/>
                </div>
            </div>
            {carouselCards.data.length === 0 && <div className = {styles.spinner}>
                <PulseLoader color = '#00EA5D'/>
            </div>}
            <div className = {styles.carouselCont}>
                <div className = {styles.carouselSecondCont}>
                    <Carousel 
                        autoPlay 
                        infiniteLoop
                        interval = {2000}
                        onChange = {handleSetActive}
                        transitionTime = {700}
                        showArrows = {false}
                        swipeable = {true}
                        showIndicators = {false}
                        showThumbs = {false}
                        emulateTouch
                    >
                        {carouselCards.data.length > 0 && carouselCards.data.map((item) =>
                            <div key = {active} className = {styles.carousel}>
                                <div className = {styles.text}>
                                    <div className = {styles.italic}>{item.description}</div>
                                    <div className = {styles.bold}>{item.name}</div>
                                    <Button>{lang.buyNow}</Button>
                                </div>
                                <img className = {styles.image} src = {item.image} alt = 'photomain'/>
                            </div>
                        )}
                    </Carousel>
                </div>
                <Dots quantity = {carouselCards.data.length} active = {active}/>
            </div>
        </div>
    )
}))
