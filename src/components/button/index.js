import React from 'react'
import styles from './index.module.sass'

export const Button = ({icon, ...props}) => 
    <div className = {styles.element} {...props}>
        {icon && <div className = {styles.iconCont}>{icon}</div>}
        {props.children}
    </div>
