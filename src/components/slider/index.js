import React, { memo, useEffect, useRef } from 'react'
import { Swiper, SwiperSlide } from 'swiper/react'
import 'swiper/css'
import styles from './index.module.sass'
import { Arrow } from '../arrow'
import { Pack } from '../pack'
import { PulseLoader } from "react-spinners"
import { GreenTitle } from '../greenTitle'
import { LazyLoader } from '../lazyImageLoader'
import { inject, observer } from 'mobx-react'



export const Slider = inject('carouselCards')(observer(({carouselCards, data, title, id, ...props}) => {

  const swiperRef = useRef(null)

  const handlePrev = () => {
    swiperRef.current.swiper.slidePrev()
  }

  const handleNext = () => {
    swiperRef.current.swiper.slideNext()
  }

  const handleAddToCart = item => {
    !carouselCards.cart.includes(item) && carouselCards.setCart(item)
  }

  return (
    <div className = {styles.mainCont}>
      {data.length > 0 && <>
        <div className = {styles.titleCont}>
          <GreenTitle>{title}</GreenTitle>
        </div>
        <div className = {styles.sliderContent}>
          <Swiper
            ref={swiperRef}
            spaceBetween={20}
            slidesPerView={5}
            // centeredSlides
            loop
            // centerInsufficientSlides
            {...props}
          >
            {data.length > 0 && data.map(item => <SwiperSlide className = {styles.cont}>
              <div className = {styles.cardCont} key = {item.id + item.name}>
                  <div className = {styles.content}>
                    <div className = {styles.imgCont}>
                      <LazyLoader>
                        <img className = {styles.image} src = {item.image} alt = 'img'/>
                      </LazyLoader>
                    </div>
                    <div className = {styles.itemName}>{item.name}</div>
                    <div className = {styles.priceCont}>
                        <div className = {styles.price}>{item.price}</div>
                        <div className = {styles.currency}>сум/кг</div>
                    </div>
                    {carouselCards.cart.find(eachItem => eachItem.id === item.id) ? 
                      <div className = {styles.addedToCart}>уже в корзине</div> :
                      <div className = {styles.packCont}>
                        <Pack onClick = {() => handleAddToCart(item)}/>
                      </div>
                    }
                  </div>
              </div>
            </SwiperSlide>)}
            {data.length > 5 && <>
              <div className = {styles.arrowCont}>
                <Arrow onClick = {handlePrev}/>
              </div>
              <div className = {styles.arrowCont}>
                  <Arrow onClick = {handleNext} isRight/>
              </div>
            </>}
          </Swiper>
        </div>
      </>}
    </div>
  )
}))
