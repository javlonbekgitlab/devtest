import React from 'react'
import arrow from '../../assets/icons/arrow.svg'
import styles from './index.module.sass'

export const Arrow = ({isRight, ...props}) => {
    return (
        <div className = {isRight ? `${styles.cont} ${styles.right}` : styles.cont} {...props}>
            <img src = {arrow} alt = 'arrow' className = {styles.arrow}/>
        </div>
    )
}