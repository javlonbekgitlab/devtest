import React, { useState } from 'react'
import { MoonLoader } from 'react-spinners'
import styles from './index.module.sass'

export const LazyLoader = ({children}) => {
    
    const { props: {src, className, alt} } = children

    const [loaded, setLoaded] = useState(false)

    const onLoad = () => setLoaded(true)

    return (
        <div className = {`${className} ${styles.cont}`}>
            <div className = {loaded ? styles.hide : styles.spinner}><MoonLoader color = '#00EA5D'/></div>
            <img src = {src} className = {loaded ? className : `${className} ${styles.hide}`} alt = {alt} onLoad = {onLoad}/>
        </div>
    )
}