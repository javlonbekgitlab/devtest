import resemble from 'resemblejs'

const rgbToLightness = (r,g,b) => 1/2 * (Math.max(r,g,b) + Math.min(r,g,b))

const rgbToSaturation = (r,g,b) => {
    const L = rgbToLightness(r,g,b)
    const max = Math.max(r,g,b)
    const min = Math.min(r,g,b)
    return (L === 0 || L === 1)
        ? 0
        : (max - min)/(1 - Math.abs(2 * L - 1))
}

const rgbToHue = (r,g,b) => Math.round(
    Math.atan2(
        Math.sqrt(3) * (g - b),
        2 * r - g - b,
    ) * 180 / Math.PI
)
    

const rgbToHsl = (r,g,b) => {
    const lightness = rgbToLightness(r,g,b)
    const saturation = rgbToSaturation(r,g,b)
    const hue = rgbToHue(r,g,b)

    return [hue, saturation, lightness]
}

export const getAvarageColourOfImage = image => {
    const { onComplete } = resemble(image)
    let rgb = {}
    onComplete(async data => rgb = data)
    // if (rgb.red) {
        return rgbToHsl(rgb.red, rgb.green, rgb.blue)
    // } else {
    //     setTimeout(() => {
    //         getAvarageColourOfImage(image)
    //     }, 100)
    // }
}