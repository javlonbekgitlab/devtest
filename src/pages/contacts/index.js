import React, { useEffect } from 'react'
import { GreenTitle } from '../../components/greenTitle'
import { lang } from '../../language/lang'
import styles from './index.module.sass'
import contactsImg from '../../assets/img/contactsImg.svg'
import { BackgroundImages } from '../../components/backgroundImages'
import { Map } from '../../components/map'

export const Contacts = () => {

    useEffect(() => {
        window.scrollTo({
            left: 0,
            behavior:'smooth',
            top: 0
        })
    }, [])

    return (
        <div className = {styles.cont}>
            <BackgroundImages/>
            <div className = {styles.content}>
                <div className = {styles.title}>{lang.contancts}</div>
                <div className = {styles.columnCont}>
                    <div className = {styles.column}>
                        <div className = {styles.text}>{lang.contactsDescription}</div>
                        <GreenTitle>{lang.workTime}</GreenTitle>
                        <div className = {styles.text}>{lang.aroundTheClock}</div>
                        <GreenTitle>{lang.ourMail}</GreenTitle>
                        <div className = {styles.text}>{'leebazar@mail.ru'}</div>
                        <div className = {styles.text}>{'leebazar@mail.ru'}</div>
                        <GreenTitle>{lang.whereAreWe}</GreenTitle>
                        <div className = {styles.text}>{lang.officeAddress}</div>
                        <GreenTitle>{lang.requisites}</GreenTitle>
                        <div className = {`${styles.text} ${styles.minText}`}>{lang.requisitesInfo}</div>
                    </div>
                    <div className = {styles.column}>
                        <img className = {styles.image} src = {contactsImg} alt = 'contact'/>
                        <div className = {styles.mapCont}>
                            <Map center = {{
                                lat: 41.311081,
                                lng: 69.240562
                            }}/>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}