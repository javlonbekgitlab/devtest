import { url } from "../../api/constants"
import { getDataFromServer } from "../../store/getDataFormServer"

export const setCarouselCards = setData => {
    getDataFromServer({url: url.getProducts, setData})
}