import React, { useEffect, useState } from 'react'
import { Advertisement } from '../../components/advertisement'
import { DiscountBlock } from '../../components/discountBlock'
import { MainCarousel } from '../../components/mainCarousel'
import { Slider } from '../../components/slider'
import { carouselCards, discountingProducts, fruits, meetProducts, vegetables } from '../../store'
import styles from './index.module.sass'
import { inject, observer } from 'mobx-react'
import { setCarouselCards } from './helperFunctions'
import { token, url } from '../../api/constants'
import { Api } from '../../api'
import { action, observable } from 'mobx'
import { ResponsiveCarousel } from '../../components/mainCarousel/responsiveCarousel'


export const MainPage = inject('carouselCards')(observer(({carouselCards}) => {

    useEffect( async() => {
        setCarouselCards(carouselCards.setData)
        window.scrollTo({
            left: 0,
            behavior:'smooth',
            top: 0
        })
    }, [])

    return (
        <div className = {styles.content}>
            <div className = {styles.mainCarouselCont}>
                {/* <MainCarousel cards = {carouselCards.data}/> */}
                <ResponsiveCarousel cards = {carouselCards.data}/>
            </div>
            <div className = {styles.sliderCont}>
                <Slider 
                    data = {carouselCards.data} 
                    title = {'Фрукты'}
                    spaceBetween = {0}
                    key = {1}
                />
            </div>
            <div className = {styles.sliderCont}>
                <Slider 
                    data = {carouselCards.data} 
                    title = {'Овощи'}
                    spaceBetween = {0}
                    key = {2}
                />
            </div>
            <div className = {styles.sliderCont}>
                <Slider 
                    data = {carouselCards.data} 
                    title = {'Мясные продукты'}
                    spaceBetween = {0}
                    key = {3}
                />
            </div>
            <div className = {styles.discountCont}>
                <DiscountBlock data = {carouselCards.data}/>
            </div>
            <div className = {styles.advertisementCont}>
                <Advertisement/>
            </div>
        </div>
    )
}))