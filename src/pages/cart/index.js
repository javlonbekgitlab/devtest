import React, { useEffect } from 'react'
import { BackgroundImages } from '../../components/backgroundImages'
import { GreenTitle } from '../../components/greenTitle'
import { MinusPlus } from '../../components/plusMinus'
import { lang } from '../../language/lang'
import { cartProducts } from '../../store'
import styles from './index.module.sass'
import clearIcon from '../../assets/icons/clear.svg'
import LazyLoad from 'react-lazyload'
import { LazyLoader } from '../../components/lazyImageLoader'
import { inject, observer } from 'mobx-react'

export const Cart = inject('carouselCards')(observer(({carouselCards}) => {

    const handleDeleteItem = id => carouselCards.deleteItemFromCart(id)

    useEffect(() => {
        window.scrollTo({
            left: 0,
            behavior:'smooth',
            top: 0
        })
    }, [])

    return (
        <div className = {styles.cont}>
            <BackgroundImages/>
            <div className = {styles.content}>
                <GreenTitle>{lang.cart}</GreenTitle>
                <div className = {styles.mapCont}>
                    {carouselCards.cart.length > 0 ? carouselCards.cart.map(item => 
                        <div className = {styles.cardItem}>
                            <div className = {styles.imgCont}>
                                <div className = {styles.infoCont}>
                                    <div className = {styles.discount}>{lang.discount}</div>
                                    <div className = {styles.discount}>{lang.popular}</div>
                                </div>
                                <LazyLoader>
                                    <img 
                                        className = {styles.image} 
                                        src = {item.image} 
                                        alt = 'prodimage'
                                        onLoad = {() => true}
                                    />
                                </LazyLoader>
                                <div className = {styles.date}>21 Фев.</div>
                            </div>
                            <div className = {styles.priceDeleteCont}>
                                <div className = {styles.priceCont}>
                                    <div className = {styles.title}>{item.name}</div>
                                    <div className = {styles.price}>
                                        <div className = {styles.sum}>{item.price}</div>
                                        <div className = {styles.currency}>{' sum'}</div>
                                    </div>
                                    <div>
                                        <MinusPlus dimension = {item.dimension}/>
                                    </div>
                                </div>
                                <div onClick = {() => handleDeleteItem(item.id)} className = {styles.iconCont}>
                                    <img className = {styles.img} src = {clearIcon} alt = 'clear'/>
                                </div>
                            </div>
                        </div>
                    ) : <div>no data</div>}
                </div>
            </div>
        </div>
    )
}))