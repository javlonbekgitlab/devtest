import React from 'react'
import { BrowserRouter, Route, Routes } from 'react-router-dom'
import { MainPage } from './pages/main'
import styles from './App.module.sass'
import { Header } from './components/header'
import { MobileAppBlock } from './components/mobileAppBlock'
import { FeedBack } from './components/feedBack'
import { Footer } from './components/footer'
import { Cart } from './pages/cart'
import { Provider } from 'mobx-react'
import { Contacts } from './pages/contacts'
import { carouselCards } from './store'

function App() {
  return (
    <Provider
      carouselCards = {carouselCards}
    >
      <BrowserRouter>
        <div className = {styles.cont}>
          <div className = {styles.headerCont}>
            <Header/>
          </div>
          <Routes>
            <Route path = '/' exact element = {<MainPage/>}/>
            <Route path = '/cart' element = {<Cart/>}/>
            <Route path = '/contacts' element = {<Contacts/>}/>
          </Routes>
          <div className = {styles.mobileBlockApp}>
              <MobileAppBlock/>
          </div>
          <div className = {styles.feedback}>
              <FeedBack/>
          </div>
          <div className = {styles.footer}>
              <Footer/>
          </div>
        </div>
      </BrowserRouter>
    </Provider>
  );
}

export default App;
